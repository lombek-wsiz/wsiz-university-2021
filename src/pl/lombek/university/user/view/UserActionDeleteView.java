package pl.lombek.university.user.view;

import pl.lombek.university.BaseView;
import pl.lombek.university.console.ConsoleReader;
import pl.lombek.university.user.domain.UserRepository;

public class UserActionDeleteView extends BaseView {

    private final UserRepository userRepository;

    public UserActionDeleteView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    protected String getTitle() {
        return "USUWANIE UŻYTKOWNIKA";
    }

    @Override
    public void initialize() {
        super.initialize();
        System.out.println("Podaj adres email:");
        String email = ConsoleReader.readString();
        userRepository.deleteByEmail(email);
        System.out.println("Użytkownik został usunięty");
    }

}
