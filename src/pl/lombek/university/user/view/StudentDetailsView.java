package pl.lombek.university.user.view;

import pl.lombek.university.console.ConsoleReader;
import pl.lombek.university.user.domain.Student;
import pl.lombek.university.user.domain.UserRepository;

public class StudentDetailsView extends UserDetailsView<Student> {

    public StudentDetailsView(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    protected String getTitle() {
        return "WSTAWIANIE STUDENTA";
    }

    @Override
    protected Student createUser() {
        System.out.println("Podaj imię:");
        String firstName = ConsoleReader.readString();

        System.out.println("Podaj nazwisko:");
        String lastName = ConsoleReader.readString();

        System.out.println("Podaj adres email:");
        String email = ConsoleReader.readString();

        System.out.println("Podaj hasło:");
        String password = ConsoleReader.readString();

        System.out.println("Podaj nr albumu:");
        long albumNumber = ConsoleReader.readLong();

        return new Student(
                firstName,
                lastName,
                email,
                password,
                albumNumber
        );
    }

}
