package pl.lombek.university.user.view;

import pl.lombek.university.BaseView;
import pl.lombek.university.user.domain.User;
import pl.lombek.university.user.domain.UserRepository;

public abstract class UserDetailsView<E extends User> extends BaseView {

    private final UserRepository userRepository;

    protected UserDetailsView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    protected abstract E createUser();

    @Override
    public void initialize() {
        super.initialize();
        E user = createUser();
        userRepository.insert(user);
        System.out.println("Użytkownik "+user+" został dodany");
    }

}
