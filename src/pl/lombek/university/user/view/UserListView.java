package pl.lombek.university.user.view;

import pl.lombek.university.ListView;
import pl.lombek.university.user.domain.User;
import pl.lombek.university.user.domain.UserRepository;

import java.util.Comparator;
import java.util.List;

public class UserListView extends ListView<User> {

    private final UserRepository userRepository;

    private final static int FIRST_NAME_MAX_LENGTH = 20;
    private final static int LAST_NAME_MAX_LENGTH = 20;
    private final static int EMAIL_NAME_MAX_LENGTH = 24;
    private final static int ROLE_NAME_MAX_LENGTH = 13;
    private final static int ALBUM_NUMBER_MAX_LENGTH = 10;
    private final static int ACADEMIC_DEGREE_MAX_LENGTH = 20;

    public UserListView(UserRepository userRepository) {
        this.userRepository = userRepository;

        addColumn("Imię", user -> user.getFirstName(), FIRST_NAME_MAX_LENGTH);
        addColumn("Nazwisko", user -> user.getLastName(), LAST_NAME_MAX_LENGTH);
        addColumn("E-mail", user -> user.getEmail(), EMAIL_NAME_MAX_LENGTH);
        addColumn("Funkcja", user -> user.getRole().getTranslated(), ROLE_NAME_MAX_LENGTH);
        addColumn("Nr albumu", user -> user.getAlbumNumber(), ALBUM_NUMBER_MAX_LENGTH);
        addColumn("Tytuł naukowy", user -> user.getAcademicDegree(), ACADEMIC_DEGREE_MAX_LENGTH);

        List<User> users = userRepository.findAll();
        users.sort(new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                int i = o1.getLastName().compareToIgnoreCase(o2.getLastName());
                if (i == 0) {
                    return o1.getFirstName().compareToIgnoreCase(o2.getFirstName());
                } else {
                    return i;
                }
            }
        });
        setItems(users);
    }

    @Override
    protected String getTitle() {
        return "LISTA UŻYTKOWNIKÓW";
    }

}
