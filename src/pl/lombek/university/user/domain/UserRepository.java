package pl.lombek.university.user.domain;

import java.util.List;

public interface UserRepository {

    void insert(User user);

    List<User> findAll();

    void deleteByEmail(String email);

}
