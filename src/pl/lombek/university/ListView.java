package pl.lombek.university;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public abstract class ListView<T> extends BaseView {

  private List<String> columns;
  private Map<String, Integer> maxLengthColumns;
  private Map<String, Function<T, ?>> valueProviderColumns;
  private List<T> items;

  public ListView() {
    columns = new ArrayList<>();
    maxLengthColumns = new HashMap<>();
    valueProviderColumns = new HashMap<>();
    items = new ArrayList<>();
  }

  protected String withSpaces(String text, int maxLength) {
    StringBuilder result = new StringBuilder(text == null ? "" : text);
    int spaces = maxLength - (text == null ? 0 : text.length());
    for (int i = 1; i <= spaces; i++) {
      result.append(" ");
    }
    return result.toString();
  }

  protected String withSpaces(Object obj, int maxLength) {
    return withSpaces(obj == null ? null : obj.toString(), maxLength);
  }

  protected void addColumn(String title, Function<T, ?> valueProvider, int maxLength) {
    this.columns.add(title);
    this.maxLengthColumns.put(title, maxLength);
    this.valueProviderColumns.put(title, valueProvider);
  }

  protected void setItems(List<T> items) {
    this.items = items;
  }

  @Override
  public void initialize() {
    super.initialize();
    for (String title: columns) {
      int maxLength = maxLengthColumns.get(title);
      System.out.print(withSpaces(title, maxLength));
      System.out.print(" ");
    }
    System.out.println();
    for (T item: items) {
      for (String title: columns) {
        int maxLength = maxLengthColumns.get(title);
        Function<T, ?> valueProvider = valueProviderColumns.get(title);
        Object value = valueProvider.apply(item);
        System.out.print(withSpaces(value, maxLength));
        System.out.print(" ");
      }
      System.out.println();
    }
  }

}
