package pl.lombek.university.subject.view;

import pl.lombek.university.ListView;
import pl.lombek.university.subject.domain.Subject;
import pl.lombek.university.subject.domain.SubjectRepository;

import java.util.List;

public class SubjectListView extends ListView<Subject> {

    private final SubjectRepository subjectRepository;

    private final static int NAME_MAX_LENGTH = 20;

    public SubjectListView(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;

        List<Subject> subjects = subjectRepository.findAll();
        subjects.sort((s1, s2) -> s1.getName().compareToIgnoreCase(s2.getName()));

        addColumn("Nazwa", subject -> subject.getName(), NAME_MAX_LENGTH);
        setItems(subjects);
    }

    @Override
    protected String getTitle() {
        return "LISTA PRZEDMIOTÓW";
    }

}
