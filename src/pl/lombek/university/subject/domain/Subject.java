package pl.lombek.university.subject.domain;

import java.io.Serializable;

public class Subject implements Serializable {

  private String name;

  public Subject(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }

}
