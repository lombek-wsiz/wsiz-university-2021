package pl.lombek.university.menu;

import pl.lombek.university.subject.domain.SubjectRepository;
import pl.lombek.university.user.domain.UserRepository;
import pl.lombek.university.user.domain.UserRole;

public class SystemMenuFactory {

    public SystemMenuView getMenuView(UserRole userRole, UserRepository userRepository,
        SubjectRepository subjectRepository) {
        SystemMenuView systemMenuView;
        switch (userRole) {
            case STUDENT:
                systemMenuView = new StudentMenuView();
                break;
            case TEACHER:
                systemMenuView = new TeacherMenuView();
                break;
            case ADMINISTRATOR:
                systemMenuView = new AdministratorMenuView(
                    userRepository, subjectRepository
                );
                break;
            default:
                throw new RuntimeException("Menu not supported");
        }
        return systemMenuView;
    }

}
