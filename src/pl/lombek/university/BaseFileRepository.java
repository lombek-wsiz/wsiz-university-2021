package pl.lombek.university;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseFileRepository<T> {

  private static final String DIRECTORY = "data";

  private final String filename;

  protected BaseFileRepository(String filename) {
    this.filename = filename;
  }

  public void insert(T item) {
    List<T> items = findAll();
    items.add(item);
    saveList(items);
  }

  protected void saveList(List<T> items) {
    FileOutputStream fileOutputStream = null;
    ObjectOutputStream objectOutputStream = null;
    try {
      File file = new File(DIRECTORY);
      file.mkdir();
      fileOutputStream = new FileOutputStream(DIRECTORY + "/" + filename);
      objectOutputStream = new ObjectOutputStream(fileOutputStream);
      objectOutputStream.writeObject(items);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (fileOutputStream != null) {
        try {
          fileOutputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      if (objectOutputStream != null) {
        try {
          objectOutputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public List<T> findAll() {
    FileInputStream fileInputStream = null;
    ObjectInputStream objectInputStream = null;
    try {
      fileInputStream = new FileInputStream(DIRECTORY + "/" + filename);
      objectInputStream = new ObjectInputStream(fileInputStream);
      Object object = objectInputStream.readObject();
      if (object instanceof List) {
        return (List<T>)object;
      }
    } catch (FileNotFoundException ignored) {
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      if (fileInputStream != null) {
        try {
          fileInputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      if (objectInputStream != null) {
        try {
          objectInputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return new ArrayList<>();
  }

}
