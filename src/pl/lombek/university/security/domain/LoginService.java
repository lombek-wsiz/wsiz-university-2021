package pl.lombek.university.security.domain;

import pl.lombek.university.user.domain.User;
import pl.lombek.university.user.domain.UserRepository;

import java.util.List;

public class LoginService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private LoggedUser loggedUser;

    public LoginService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User checkCredentials(String email, String password) throws BadCredentialsException {
        List<User> users = userRepository.findAll();
        for (User user : users) {
            if (user.getEmail().equalsIgnoreCase(email)
                    && user.getPassword().equals(passwordEncoder.encode(password))) {
                return user;
            }
        }
        throw new BadCredentialsException("No found user with email = " + email);
    }

    public void saveUser(User user) {
        loggedUser = new LoggedUser(
                user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getRole()
        );
    }

    public LoggedUser getLoggedUser() {
        return loggedUser;
    }

}
